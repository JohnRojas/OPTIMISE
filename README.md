_______________________________________________________________________________
# OPTIMISED (OPTIMIZED Summary Evaluation)
#### Summary evaluation based on the linear optimization of content metrics
_______________________________________________________________________________

### Authors of this repo:
  * Jonathan Rojas Simón (IDS_Jonathan_Rojas@hotmail.com)
  * Yulia Ledeneva (yledeneva@yahoo.com)
  * René Arnulfo García-Hernández (renearnulfo@hotmail.com)

_______________________________________________________________________________

#### Overview
_______________________________________________________________________________
OPTIMISED is an evaluation package based on the linear optimization of content
metrics. Such optimization was carried out via Genetic Algorithms in order to 
improve the correlation between automatic evaluation and manual assessment. 

The content metrics included in this package derive from the following evaluation 
packages:
  * ROUGE-C [(He et al., 2008)]
  * Latent Semantic Analysis (LSA) [(Steinberger and Ježek, 2009)]
  * SIMetrix [(Louis & Nenkova, 2013)]
_______________________________________________________________________________

#### Contents of this repo
_______________________________________________________________________________

The contents of this repo are the following:
  * Content 1
  * Content 2
  * Content 3
  * Content 4

_______________________________________________________________________________

#### Citation
_______________________________________________________________________________

If you use OPTIMIZED, please cite it as follows:

Rojas-Simón, J., Ledeneva, Y., & García-Hernández, R. A. (2020). Evaluation of 
Text Summar ies without Human References based on the Linear Optimization of 
Content Metrics using a Genetic Algorithm. Expert Systems with Applications, 
113827, https://doi.org/10.1016/j.eswa.2020.113827.

```
@article{ROJASSIMON2020113827,
title = "Evaluation of Text Summaries without Human References based on the Linear Optimization of Content Metrics using a Genetic Algorithm",
journal = "Expert Systems with Applications",
pages = "113827",
year = "2020",
issn = "0957-4174",
doi = "https://doi.org/10.1016/j.eswa.2020.113827",
url = "http://www.sciencedirect.com/science/article/pii/S0957417420306394",
author = "Jonathan Rojas-Simón and Yulia Ledeneva and René {Arnulfo García-Hernández}"
}

```
About individual evaluation packages, please cite them as follows: 

  * [(He et al., 2008)] - He T., Chen J., Ma L., Gui Z., Li F., Shao W., Wang Q., ROUGE-C: A fully automated evaluation method for multi-document summarization IEEE International Conference on Granular Computing 2008 269-274, https://doi.org/10.1109/GRC.2008.4664680.
  * [(Steinberger and Ježek, 2009)] - Steinberger J., Ježek K., Evaluation measures for text summarization Computing and Informatics 2009 251-275.
  * [(Louis & Nenkova, 2013)] - Louis A., Nenkova A., Automatically assessing machine summary content without a gold standard Computational Linguistics 2013 267-300, https://doi.org/10.1162/COLI_a_00123.

  [(He et al., 2008)]: <https://ieeexplore.ieee.org/abstract/document/4664680>
  [(Steinberger and Ježek, 2009)]: <http://www.cai.sk/ojs/index.php/cai/article/viewArticle/37>
  [(Louis & Nenkova, 2013)]: <https://www.mitpressjournals.org/doi/full/10.1162/COLI_a_00123>
